from django.urls import path, include
from rest_framework import routers

from . import views

router = routers.DefaultRouter()
router.register(r'tasks', views.TaskViewSet)

urlpatterns = [
    path('', include(router.urls)),
    path('tasks-1/', views.task_list),
    path('tasks/<int:pk>/', views.task_detail),
]

from rest_framework import status
from rest_framework.test import APIClient
from django.urls import reverse
import pytest

from todo.models import Task


@pytest.mark.django_db
def test_create_task():
    client = APIClient()
    data = {'title': 'Test Task', 'description': 'Example description'}
    response = client.post(reverse('task-list'), data, format='json')
    assert response.status_code == status.HTTP_201_CREATED
    assert response.data['title'] == 'Test Task'


@pytest.mark.django_db
def test_get_task_list():
    client = APIClient()
    response = client.get(reverse('task-list'), format='json')
    assert response.status_code == status.HTTP_200_OK
    assert len(response.data) == 1


@pytest.mark.django_db
def test_get_task_detail():
    client = APIClient()
    task = Task.objects.create(title='Test Task')
    response = client.get(reverse('task-detail', args=[task.id]), format='json')
    assert response.status_code == status.HTTP_200_OK
    assert response.data['title'] == 'Test Task'


@pytest.mark.django_db
def test_update_task():
    client = APIClient()
    task = Task.objects.create(title='Test Task')
    data = {'title': 'Updated Test Task', 'completed': True}
    response = client.put(reverse('task-detail', args=[task.id]), data, format='json')
    assert response.status_code == status.HTTP_200_OK
    assert response.data['title'] == 'Updated Test Task'
    assert response.data['completed'] == True


@pytest.mark.django_db
def test_delete_task():
    client = APIClient()
    task = Task.objects.create(title='Test Task')
    response = client.delete(reverse('task-detail', args=[task.id]), format='json')
    assert response.status_code == status.HTTP_204_NO_CONTENT
    assert Task.objects.count() == 0
